<!-- ABOUT THE PROJECT -->
## About The Project
 
A simple E-commerce application where user can buy project from store.
Feature:
1. login & register
2. filter product as per category
3. can add product to category
4. can see the summery of final order
5. can checkout product 
 
### Built With
 
* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
 
### Installation
 
1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 manage.py makemigrations`
7. type `python3 manage.py migrate`
8. type `python3 manage.py runserver`

### Project Snap

#### Home Page
![Screenshot_2020-12-24_at_11.09.47_AM](/uploads/baaa3a2958af9e0eabcd51c755138e40/Screenshot_2020-12-24_at_11.09.47_AM.png)

#### Cart Page
![Screenshot_2020-12-24_at_11.13.10_AM](/uploads/6c2028df8488457da30db558362a0528/Screenshot_2020-12-24_at_11.13.10_AM.png)

#### Login and Register Page
![Screenshot_2020-12-24_at_11.17.39_AM](/uploads/5a69462da47c5f989a7edb68753f6838/Screenshot_2020-12-24_at_11.17.39_AM.png)

#### order Summary
![Screenshot_2020-12-24_at_11.22.14_AM](/uploads/2b0334797af8143bdf52990c6bdc1072/Screenshot_2020-12-24_at_11.22.14_AM.png)
